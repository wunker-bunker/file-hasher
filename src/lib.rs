mod utils;

use wasm_bindgen::prelude::*;
use std::fs;
use sha2::{Sha256, Digest};
use rayon::prelude::*;
use memmap::MmapOptions;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}


fn hash_file(filename: &str) -> Result<String, String> {
    let file = match fs::OpenOptions::new().read(true).open(filename) {
        Ok(file) => file,
        Err(_) => return Err("Couldn't open file".to_string()),
    };

    let mmap = match unsafe { MmapOptions::new().map(&file) } {
        Ok(mmap) => mmap,
        Err(err) => return Err("Couldn't memory map file: ".to_owned() + &err.to_string()),
    };

    let hash = Sha256::digest(&mmap);
    let hash_hex = format!("{:x}", hash);

    Ok(hash_hex)
}

// Create the main function that takes in one or more filenames and prints the hashes
#[wasm_bindgen]
pub fn hash_files() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} FILENAME", args[0]);
        return;
    }

    let filenames = &args[1..];

    filenames.par_iter().for_each(|filename| {
        match hash_file(&filename) {
            Ok(hash) => println!("{}: {}", filename, hash),
            Err(err) => println!("{}: {}", filename, err),
        }
    });

    // Print the number of files hashed
    println!("Hashed {} files", filenames.len());
}